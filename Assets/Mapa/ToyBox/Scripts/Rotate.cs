﻿using UnityEngine;
using System.Collections;
using Photon.Pun;

//[RequireComponent(typeof(PhotonView), typeof(PhotonTransformView))]
public class Rotate : MonoBehaviourPunCallbacks
{

	public float speed;
	public bool usarPhoton = true;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (usarPhoton)
        {
			if (!PhotonNetwork.IsMasterClient) return;
		}
		transform.Rotate(Vector3.up * Time.deltaTime * speed * 10);
	}
}
