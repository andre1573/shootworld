﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

[RequireComponent(typeof(PhotonView), typeof(PhotonTransformView), typeof(PhotonAnimatorView))]
public class TrapDoorDemo : MonoBehaviour {

    //This script goes on the TrapDoor prefab;

    public Animator TrapDoorAnim; //Animator for the trap door;
    public int tempoOpen = 2, tempoClose = 2, delay = 0;
    private bool iniciouCoroutine = false;

    // Use this for initialization
    void Awake()
    {
        //get the Animator component from the trap;
        TrapDoorAnim = GetComponent<Animator>();
        //start opening and closing the trap for demo purposes;
        StartCoroutine(OpenCloseTrap());
    }


    IEnumerator OpenCloseTrap()
    {
        if(!iniciouCoroutine)
        {
            iniciouCoroutine = true;
            yield return new WaitForSeconds(delay);
        }
        
        if (PhotonNetwork.IsMasterClient)
        {
            //play open animation;
            TrapDoorAnim.SetTrigger("open");
        }
        //wait 2 seconds;
        yield return new WaitForSeconds(tempoClose);
        if (PhotonNetwork.IsMasterClient)
        {
            //play close animation;
            TrapDoorAnim.SetTrigger("close");
        }
        //wait 2 seconds;
        yield return new WaitForSeconds(tempoOpen);
        //Do it again;
        StartCoroutine(OpenCloseTrap());
    }
}