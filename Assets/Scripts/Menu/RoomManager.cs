﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.SceneManagement;
using System.IO;

public class RoomManager : MonoBehaviourPunCallbacks
{

	public bool competitive = false;
	protected static RoomManager s_instance;

	protected static RoomManager Instance
	{
		get
		{
			if (s_instance == null)
			{
				return new GameObject("RoomManager").AddComponent<RoomManager>();
			}
			else
			{
				return s_instance;
			}
		}
	}

	void Awake()
	{
		if (s_instance != null)
		{
			Destroy(gameObject);
			return;
		}
		s_instance = this;
		DontDestroyOnLoad(gameObject);
	}

	public override void OnEnable()
	{
		base.OnEnable();
		SceneManager.sceneLoaded += OnSceneLoaded;
	}

	public override void OnDisable()
	{
		base.OnDisable();
		SceneManager.sceneLoaded -= OnSceneLoaded;
	}

	void OnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
	{
		if(scene.name.Contains("Game")) // We're in the game scene
		{
			Destroy(GameObject.Find("PlayerManager"));
			PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "PlayerManager"), Vector3.zero, Quaternion.identity);
			PlayerPrefs.SetString("ultimoMapa", scene.name);
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = false;
		}
        else
        {
			Cursor.lockState = CursorLockMode.Confined;
			Cursor.visible = true;
			if (scene.name == "FimDeJogo")
            {
                if (PhotonNetwork.CurrentLobby.Name == "competitive")
                {
					Debug.Log("LeaveRoom() - Desconectando da room...");
					PhotonNetwork.LeaveRoom();
				}
            }else if (scene.name == "Menu")
            {
				Debug.Log("Chegou no menu");
			}
		}
	}
}