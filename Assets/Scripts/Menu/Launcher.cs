﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon;
using Photon.Realtime;
using TMPro;
using System.Linq;
using UnityEngine.SceneManagement;
using ExitGames.Client.Photon;

public class Launcher : MonoBehaviourPunCallbacks
{
	public static Launcher Instance;

	[SerializeField] TMP_InputField roomNameInputField, roomNameFiltro, roomPasswordInputField;
	[SerializeField] TMP_Text errorText;
	[SerializeField] TMP_Text roomNameText, roomMaxPlayersText;
	[SerializeField] Transform roomListContent;
	[SerializeField] GameObject roomListItemPrefab;
	[SerializeField] Transform playerListContent;
	[SerializeField] GameObject PlayerListItemPrefab;
	[SerializeField] GameObject startGameButton, playCompetitiveGameButton, unavailableCompetitiveGameButton, voltarPraSalaButton;
	[SerializeField] Mapas mapa;
	private bool isStartou = false, modoBunnyhop = false, modoCompetitivo = false;
	private byte maxPlayers = 10;
	public const string GAME_MODE_COMPETITIVE_VALUE = "competitive";
	public const string GAME_MODE_FUN_VALUE = "fun";
	public const string GAME_MODE_BUNNYHOP_VALUE = "bunnyhop";

	private List<RoomInfo> roomListCache;
	public bool isSalaPrivada;
	[HideInInspector] public SaveManager sm;

	void Awake()
	{
		Instance = this;
	}
	void Start()
	{
		if (SceneManager.GetActiveScene().name.Contains("Game")) return;
		Debug.Log("Start - Offline mode: " + PhotonNetwork.OfflineMode);
		if (!PhotonNetwork.IsConnected)
		{
			string chaveRegion = ObterChaveRegionPorIndex();
			Debug.Log("Connecting to Master: " + chaveRegion);
			AppSettings settings = new AppSettings();
			settings.AppIdRealtime = "38d0d89f-1b27-49fc-8b57-947e6cdd6148";
			settings.AppVersion = "1.0";
			settings.Protocol = ConnectionProtocol.Udp;
			settings.FixedRegion = chaveRegion;
			settings.UseNameServer = true;
			PhotonNetwork.ConnectUsingSettings(settings); //sem setar as settings o photon usa o arquivo ServerPhotonSettings como padrao, e setando as settings, precisa preencher a entidade
        }
        else
        {
			if (PhotonNetwork.InRoom)
			{
				MenuManager.Instance.OpenMenu("room");
				AtualizarPlayersInRoom();
			}
			else
			{
				MenuManager.Instance.OpenMenu("title");
			}
		}
		isStartou = false;
	}

    private void LateUpdate()
    {
        if(sm != null && playCompetitiveGameButton != null)
        {
            if (playCompetitiveGameButton.activeSelf && !sm.competitivoLiberado) //Modo competitivo indisponivel via playfab title data (competitivoLiberado)
			{

				playCompetitiveGameButton.SetActive(false);
				unavailableCompetitiveGameButton.SetActive(true);
			}
			if (!playCompetitiveGameButton.activeSelf && sm.competitivoLiberado) //Modo competitivo disponivel via playfab title data (competitivoLiberado)
			{
				playCompetitiveGameButton.SetActive(true);
				unavailableCompetitiveGameButton.SetActive(false);
			}
		}
		if (voltarPraSalaButton != null && PhotonNetwork.CurrentLobby.Name == null)
		{
			voltarPraSalaButton.SetActive(PhotonNetwork.IsMasterClient);
		}
	}

    public void AtualizarPlayersInRoom()
    {
		Player[] players = PhotonNetwork.PlayerList;
		Debug.Log("Instanciando player list item: " + players.Length);
		if (playerListContent != null)
		{
			foreach (Transform child in playerListContent)
			{
				Destroy(child.gameObject);
			}

			if (PlayerListItemPrefab != null)
			{
				for (int i = 0; i < players.Count(); i++)
				{
					Instantiate(PlayerListItemPrefab, playerListContent).GetComponent<PlayerListItem>().SetUp(players[i]);
				}
			}
		}
		if (roomNameText != null) roomNameText.text = PhotonNetwork.CurrentRoom.Name;
		if (startGameButton != null) startGameButton.SetActive(PhotonNetwork.IsMasterClient);
	}

	private string ObterChaveRegionPorIndex()
    {
		if (PlayerPrefs.GetInt("INDEXSERVERREGION") == 0) //USA, East
		{
			return "us";
		}
		else if (PlayerPrefs.GetInt("INDEXSERVERREGION") == 1) //South America
		{
			return "sa";
		}
		else if (PlayerPrefs.GetInt("INDEXSERVERREGION") == 2) //Europe
		{
			return "eu";
		}
		else if (PlayerPrefs.GetInt("INDEXSERVERREGION") == 3) //Asia
		{
			return "asia";
		}
		else if (PlayerPrefs.GetInt("INDEXSERVERREGION") == 4) //Australia
		{
			return "au";
		}
		else if (PlayerPrefs.GetInt("INDEXSERVERREGION") == 5) //Japan
		{
			return "jp";
		}
		else
		{
			return "us"; ////USA, East
		}
	}

	public override void OnConnectedToMaster()
	{
		Debug.Log("Connected to Master: "+PhotonNetwork.CloudRegion);
		if (!PhotonNetwork.InLobby)
        {
			string gamemode = PlayerPrefs.GetString("gamemode");
			TypedLobby typelobby = new TypedLobby(gamemode, LobbyType.SqlLobby);
			PhotonNetwork.JoinLobby(typelobby);
			PhotonNetwork.AutomaticallySyncScene = true;
		}
	}

    public override void OnJoinedLobby() //Apos conectado no photon, entra no lobby e abre o menu online
	{
		if (SceneManager.GetActiveScene().name.Contains("Game")) return;
		PhotonNetwork.DestroyAll(false);
		if (PhotonNetwork.CurrentLobby.Name == GAME_MODE_BUNNYHOP_VALUE)
		{
			CreateBunnyhopRoom();
		}else if(PhotonNetwork.CurrentLobby.Name == GAME_MODE_COMPETITIVE_VALUE)
        {
			MenuManager.Instance.OpenMenu("title");
        }
        else
        {
			MenuManager.Instance.OpenMenu("find room");
		}
		if (sm == null)
		{
			GameObject objSaveManager = GameObject.FindGameObjectWithTag("SaveManager");
			if (objSaveManager != null)
			{
				sm = objSaveManager.GetComponent<SaveManager>();
			}
		}
		if (sm != null) sm.GetContentPlayfabTitleDataCompetitivoLiberado();
		Debug.Log("INPARTIDA = " + PlayerPrefs.GetInt("inpartida"));
		if (PlayerPrefs.GetInt("inpartida") == 1)
		{
			GameObject objLeaderboard = GameObject.FindGameObjectWithTag("menucontroller"); 
			if(objLeaderboard != null) objLeaderboard.GetComponent<PlayFabLeaderboard>().SendLeaderBoardTrofeuScoreForce(-20);
			PlayerPrefs.SetInt("inpartida", 0);
			SceneManager.LoadScene("FimDeJogo");
			Debug.Log("Voce kitou na ultima partida e perdeu 20 trofeus");
		}
		Debug.Log("Joined Lobby "+ PhotonNetwork.CurrentLobby.Name);
	}

	public void CreateFunGameRoom() //CRIANDO SALA FUN
    {
		modoCompetitivo = false;
		modoBunnyhop = false;
		RoomOptions roomOptions = CriarRoomOptions(maxPlayers);
		Debug.Log("Sala FUN criada max players: " + maxPlayers);
		if(!string.IsNullOrEmpty(roomPasswordInputField.text))
        {
			ExitGames.Client.Photon.Hashtable customProperties = new ExitGames.Client.Photon.Hashtable();
			customProperties.Add("secret", roomPasswordInputField.text);
			roomOptions.CustomRoomProperties = customProperties;
			roomOptions.CustomRoomPropertiesForLobby = new string[] { "secret" };
		}
		CreateRoom(roomOptions);
		MenuManager.Instance.OpenMenu("loading");
	}
	

	public void CreateCompetitiveRoom() //CRIANDO SALA COMPETITIVO
	{
		modoCompetitivo = true;
		RoomOptions roomOptions = CriarRoomOptions(maxPlayers);
		CreateRoom(roomOptions);
		MenuManager.Instance.OpenMenu("findmatch");
	}

	public void CreateBunnyhopRoom() //CRIANDO SALA COMPETITIVO
	{
		maxPlayers = 1;
		modoCompetitivo = false;
		modoBunnyhop = true;
		RoomOptions roomOptions = CriarRoomOptions(maxPlayers);
		roomOptions.IsOpen = false;
		roomOptions.IsVisible = false;
		CreateRoom(roomOptions);
		MenuManager.Instance.OpenMenu("loading");
	}

	public void CreateRoom(RoomOptions roomOptions) //USADO PARA FUN E COMPETITIVO
	{
		if (SceneManager.GetActiveScene().name.Contains("Game")) return;
		if (string.IsNullOrEmpty(roomNameInputField.text))
		{
			roomNameInputField.text = PhotonNetwork.NickName + "'s room - " +Random.Range(0, 100000);
		}
		Debug.Log("Criando room");
		PhotonNetwork.CreateRoom(roomNameInputField.text, roomOptions);
	}

	public void JoinOrCreateRoom() //NAO TA SENDO USADO
	{
		if (SceneManager.GetActiveScene().name.Contains("Game")) return;
		//PhotonNetwork.NickName = PlayerPrefs.GetString("NICKNAME") != null && PlayerPrefs.GetString("NICKNAME") != "" ? PlayerPrefs.GetString("NICKNAME") : "Player " + Random.Range(0, 1000).ToString("0000");
		if (string.IsNullOrEmpty(roomNameInputField.text))
		{
			roomNameInputField.text = PhotonNetwork.NickName + "'s room -" + Random.Range(0, 100000);
		}
		
		PhotonNetwork.JoinOrCreateRoom("Training", CriarRoomOptions(1), TypedLobby.Default);
		MenuManager.Instance.OpenMenu("loading");
	}

	public override void OnCreatedRoom()
    {
		Debug.Log("Sala criada");
		if (modoBunnyhop)
        {
			StartGame();
        }else if (!modoCompetitivo)
        {
			MenuManager.Instance.OpenMenu("room");
			AtualizarPlayersInRoom();
		}
    }

	public override void OnJoinedRoom()
	{
		if (SceneManager.GetActiveScene().name.Contains("Game")) return;
        if (PhotonNetwork.OfflineMode)
        {
			Debug.Log("Startando game no modo offline");
			StartGame();
        }
        else
        {
			if (SceneManager.GetActiveScene().name.Contains("Game")) return;
            if (!modoCompetitivo && !modoBunnyhop)
            {
				MenuManager.Instance.OpenMenu("room");
				AtualizarPlayersInRoom();
            }
		}
	}

	public override void OnMasterClientSwitched(Player newMasterClient)
	{
		if(startGameButton != null)
        {
			startGameButton.SetActive(PhotonNetwork.IsMasterClient);
		}
		if(voltarPraSalaButton != null && PhotonNetwork.InLobby && PhotonNetwork.CurrentLobby.Name == null)
        {
			voltarPraSalaButton.SetActive(PhotonNetwork.IsMasterClient);
        }
	}

	public override void OnCreateRoomFailed(short returnCode, string message)
	{
		if (SceneManager.GetActiveScene().name.Contains("Game")) return;
		errorText.text = "Room Creation Failed: " + message;
		Debug.LogError("Room Creation Failed: " + message);
		MenuManager.Instance.OpenMenu("error");
	}

	public void StartGame()
	{
		if (!PhotonNetwork.IsMasterClient) return;
		if (SceneManager.GetActiveScene().name.Contains("Game")) return;
		Debug.Log("Iniciando partida");
		if (isStartou) return;
		MenuManager.Instance.OpenMenu("loading");
		isStartou = true;
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
		int indexMapaAleatorio = Random.Range(0, mapa.imgMapas.Length);
        if (modoBunnyhop)
        {
			PhotonNetwork.LoadLevel("GameBunnyhop");
		}
        else
        {
			if (!modoCompetitivo) //is Modo FUN
			{
				indexMapaAleatorio = mapa.index;
			}
			PhotonNetwork.LoadLevel("Game" + indexMapaAleatorio);
		}
	}

	public void LeaveRoom()
	{
		if (SceneManager.GetActiveScene().name.Contains("Game"))
		{
			if (PhotonNetwork.CurrentLobby.Name == GAME_MODE_COMPETITIVE_VALUE)
			{
				foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
				{
					if (player.GetComponent<PhotonView>().IsMine)
					{
						player.GetComponent<ControleCorrida>().PlayerKitouDoCompetitivo();
						return;
					}
				}
            }
            else
            {
                if (PhotonNetwork.InRoom)
                {
					PhotonNetwork.LeaveRoom();
                }
                else
                {
					PhotonNetwork.LoadLevel("Menu");
				}
			}
        }
        else
        {
            if (PhotonNetwork.InRoom)
            {
				PhotonNetwork.LeaveRoom();
				MenuManager.Instance.OpenMenu("loading");
			}
            else
            {
				PhotonNetwork.LoadLevel("Menu");
			}
		}
	}

	public void LeaveRoomFunMode()
	{
		PlayerPrefs.SetString("gamemode", GAME_MODE_FUN_VALUE);
		PhotonNetwork.LeaveRoom();
		if (SceneManager.GetActiveScene().name.Contains("Game")) return;
		MenuManager.Instance.OpenMenu("loading");
	}

	public void JoinRoom(RoomInfo info)
	{
		PhotonNetwork.JoinRoom(info.Name);
		if (SceneManager.GetActiveScene().name.Contains("Game")) return;
		MenuManager.Instance.OpenMenu("loading");
	}

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
		MenuManager.Instance.OpenMenu("find room");
		base.OnJoinRoomFailed(returnCode, message);
    }

    public override void OnLeftRoom()
	{
		Debug.Log("Saiu da room");
		if ((SceneManager.GetActiveScene().name == "Menu"))
        {
            if (PlayerPrefs.GetString("gamemode") == GAME_MODE_COMPETITIVE_VALUE)
            {
                MenuManager.Instance.OpenMenu("title");
            }
            else
            {
                MenuManager.Instance.OpenMenu("find room");
            }
        }
        else
        {
			Debug.Log("leftroom else");
			if(PhotonNetwork.CurrentLobby.Name == GAME_MODE_BUNNYHOP_VALUE)
            {
				PlayerPrefs.SetString("gamemode", GAME_MODE_COMPETITIVE_VALUE);
				PhotonNetwork.LoadLevel("Menu");
			}
			else if (PhotonNetwork.CurrentLobby.Name != GAME_MODE_COMPETITIVE_VALUE || SceneManager.GetActiveScene().name.Contains("Game")) //modo FUN
            {
				Debug.Log("LOAD ELVEL MENUUUU");
				PhotonNetwork.LoadLevel("Menu");
            }
        }
		

		base.OnLeftRoom();
	}

	public override void OnRoomListUpdate(List<RoomInfo> roomList)
	{
		if (SceneManager.GetActiveScene().name.Contains("Game")) return;
		if (roomListContent == null) return;
		Debug.Log("Atualizando lista de servidores");

		foreach (Transform trans in roomListContent)
		{
			Destroy(trans.gameObject);
		}

		for (int i = 0; i < roomList.Count; i++)
		{
			if (roomList[i].RemovedFromList)
				continue;

			Instantiate(roomListItemPrefab, roomListContent).GetComponent<RoomListItem>().SetUp(roomList[i]);
		}
		roomListCache = roomList;
	}

	public void filtrarRoomList()
	{
		foreach (Transform trans in roomListContent)
		{
			Destroy(trans.gameObject);
		}

		for (int i = 0; i < roomListCache.Count; i++)
		{
			if (roomNameFiltro.text == null || roomNameFiltro.text == "" || roomListCache[i].Name.Contains(roomNameFiltro.text)) //filtro sala por nome
			{
				Instantiate(roomListItemPrefab, roomListContent).GetComponent<RoomListItem>().SetUp(roomListCache[i]);
			}
		}
	}

	public void BackFunGameMode()
    {
		PlayerPrefs.SetString("gamemode", GAME_MODE_COMPETITIVE_VALUE);
		TypedLobby typelobby = new TypedLobby(GAME_MODE_COMPETITIVE_VALUE, LobbyType.SqlLobby);
		PhotonNetwork.JoinLobby(typelobby);
	}

	public void StartBunnyhopGame() //FUN GAME: Modo de jogar com amigos
	{
		GameObject.FindGameObjectWithTag("roommanager").GetComponent<RoomManager>().competitive = false;
		PlayerPrefs.SetString("gamemode", GAME_MODE_BUNNYHOP_VALUE);
		TypedLobby typelobby = new TypedLobby(GAME_MODE_BUNNYHOP_VALUE, LobbyType.SqlLobby);
		PhotonNetwork.JoinLobby(typelobby);
	}
	public void StartFunGame() //FUN GAME: Modo de jogar com amigos
    {
		modoCompetitivo = false;
		modoBunnyhop = false;
		PlayerPrefs.SetString("gamemode", GAME_MODE_FUN_VALUE);
		//TypedLobby typelobby = new TypedLobby(GAME_MODE_FUN_VALUE, LobbyType.SqlLobby);
		GameObject.FindGameObjectWithTag("roommanager").GetComponent<RoomManager>().competitive = false;
		PhotonNetwork.JoinLobby();
	}

	public void FindMatchMaking1() //MODO COMPETITIVO: 
    {
		modoCompetitivo = true;
		modoBunnyhop = false;
		maxPlayers = 2;
		GameObject.FindGameObjectWithTag("roommanager").GetComponent<RoomManager>().competitive = true;
		FindMatchmaking();
	}

	private void FindMatchmaking() //Deixar sala nao visivel
    {
		Debug.Log("Finding game matchmaking");
		MenuManager.Instance.OpenMenu("loading");
		PhotonNetwork.JoinRandomRoom(null, 2); //competitivo sao 2 players
	}

	private RoomOptions CriarRoomOptions(byte maxPlayers)
	{
		RoomOptions roomOptions = new RoomOptions();
		roomOptions.IsOpen = true;
		roomOptions.IsVisible = true; //se botar false nao encontra nem no find do matchmaking
		roomOptions.MaxPlayers = maxPlayers;
		roomOptions.PublishUserId = true;
		roomOptions.CleanupCacheOnLeave = true;
		roomOptions.PlayerTtl = 1000; //verificar isso, qual o melhor pr
		return roomOptions;
	}

	public void StopSearch()
    {
		PhotonNetwork.LeaveRoom();
    }

	public override void OnJoinRandomFailed(short returneCode, string message) //NAO ENCONTROU UMA SALA COMPETITIVE
    {
		Debug.Log("Nao encontrou nenhuma sala, criando uma...");
		CreateCompetitiveRoom();
	}

	public override void OnPlayerEnteredRoom(Player newPlayer)
	{
		Debug.Log("O player " + newPlayer.NickName + " entrou na sala");
		if (SceneManager.GetActiveScene().name.Contains("Game")) { //ALGUM PLAYER ENTROU NO GAME COM A PARTIDA ROLANDO
			GameObject.FindGameObjectWithTag("colocacaocontrole").GetComponent<ColocacaoControle>().EnviarInformacoesDaCorrida();
            if (modoCompetitivo)
            {
				GameObject.FindGameObjectWithTag("roommanager").GetComponent<RoomManager>().competitive = true;
            }
            else
            {
				GameObject.FindGameObjectWithTag("roommanager").GetComponent<RoomManager>().competitive = false;
			}
        }
        else
        {
			if (modoCompetitivo)
			{
				if (PhotonNetwork.CurrentRoom.PlayerCount == PhotonNetwork.CurrentRoom.MaxPlayers && PhotonNetwork.IsMasterClient)
				{
					Debug.Log("A sala tem o maximo de players, iniciando game...");
					StartGame();
				}
			}
			else //MODO FUN O START EH MANUAL
			{
				//if (PlayerListItemPrefab != null && playerListContent != null) Instantiate(PlayerListItemPrefab, playerListContent).GetComponent<PlayerListItem>().SetUp(newPlayer);
				MenuManager.Instance.OpenMenu("room");
				AtualizarPlayersInRoom();
			}
		}
	}

	public override void OnLeftLobby()
	{
		Debug.Log("Saiu do Lobby");
		foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
		{
			if (player.GetComponent<PhotonView>().IsMine)
			{
				PhotonNetwork.Destroy(player);
			}
		}
		if (PhotonNetwork.IsConnected)
		{
			PhotonNetwork.Disconnect();
		}
		PhotonNetwork.LoadLevel(0);
		base.OnLeftLobby();
	}

	public override void OnPlayerLeftRoom(Player playerLeft)
    {
		Debug.Log("Player Disconnected left room: " + playerLeft.NickName+ " in scene "+ SceneManager.GetActiveScene().name);
		if (SceneManager.GetActiveScene().name.Contains("Game"))
        {
			if(PhotonNetwork.CurrentLobby.Name == GAME_MODE_COMPETITIVE_VALUE) { //MODO COMPETITIVO
				Debug.Log("Só tem um player na sala... finalizando game");
				foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
				{
					PhotonView PV = player.GetComponent<PhotonView>();
					if(PhotonNetwork.CurrentRoom.PlayerCount <= 1) //so tem 1 player na sala... finalziando game
                    {
						if (PV != null && PV.IsMine && PhotonNetwork.IsMasterClient)
						{
							Debug.Log("Encontrado o Player restante na sala...");
							player.GetComponent<ControleCorrida>().vencedor = true;
							player.GetComponent<ControleCorrida>().PlayerChegouNaChegadaFinal(false);
							return;
						}
					}
				}
            }
            /*else //tirar isso e testar
            {
				Debug.Log("Tem "+PhotonNetwork.CurrentRoom.PlayerCount+" na sala");
				foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
                {
					if(player.GetComponent<PhotonView>().Owner.UserId == playerLeft.UserId)
                    {
						PhotonNetwork.DestroyPlayerObjects(player.GetComponent<PhotonView>().ViewID, false);
					}
				}
			}*/
		}
	}

	public void VoltarAoMenuPrincipal()
	{
		if (PhotonNetwork.IsConnected)
		{
			PhotonNetwork.Disconnect();
		}
		SceneManager.LoadScene("MenuPrincipal");
	}

	public void VoltarAoMenu()
	{
		SceneManager.LoadScene("Menu");
	}

	public void SairTelaFimDeJogo()
    {
        if (PhotonNetwork.InRoom)
        {
			LeaveRoom();
        }
        else
        {
			PhotonNetwork.LoadLevel("Menu");
		}
		
	}

	public void SairDoGame()
    {
		if (PhotonNetwork.InRoom)
        {
			LeaveRoom();
		}
		PhotonNetwork.LoadLevel("Menu");
	}

	public void VoltarPraSala()
    {
		SceneManager.LoadScene("Menu");
    }

	public void MasterLevaTodoMundoPraSala()
    {
        if (PhotonNetwork.IsMasterClient)
        {
			PhotonNetwork.LoadLevel("Menu");
		}
	}

	public void PlayAgain()
    {
		if(PhotonNetwork.InRoom && PhotonNetwork.CurrentLobby.Name != GAME_MODE_COMPETITIVE_VALUE) //FUN
        {
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = false;
			PhotonNetwork.LoadLevel(PlayerPrefs.GetString("ultimoMapa"));
        }
    }

	public void SairDoJogo()
	{
		Application.Quit();
	}

	public void setMaxPlayersSlider(float value)
	{
		maxPlayers = (byte)((int)value);
		roomMaxPlayersText.text = maxPlayers+"";
		Debug.Log("Maxplayers: " + maxPlayers);
	}

}