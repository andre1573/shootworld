using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using TMPro;

public class Settings : MonoBehaviour
{

    public Toggle fullScreenToggle;
    public Slider sensivitySlider;
    public AudioMixer audioMixer;
    Resolution[] resolutions;
    public TMP_Dropdown resolutionDropdown;
    public Slider volumeSlider;

    // Start is called before the first frame update
    void Start()
    {
        sensivitySlider.value = PlayerPrefs.GetFloat("sensivity") > 0 ? PlayerPrefs.GetFloat("sensivity") : 2.0f;
        volumeSlider.value = PlayerPrefs.GetFloat("volume");
        fullScreenToggle.isOn = Screen.fullScreen;

        resolutions = Screen.resolutions;
        resolutionDropdown.ClearOptions();
        List<string> options = new List<string>();
        int currentResolutionIndex = PlayerPrefs.GetInt("resolutionIndex");
        for (int i = 0; i < resolutions.Length; i++)
        {
            string option = resolutions[i].width + "x" + resolutions[i].height;
            if(!options.Contains(option)) options.Add(option);
        }
        resolutionDropdown.AddOptions(options);
        resolutionDropdown.value = currentResolutionIndex;
        resolutionDropdown.RefreshShownValue();

        resolutions = Screen.resolutions;
        Resolution resolution = resolutions[resolutions.Length - 1];
        Screen.SetResolution(resolution.width, resolution.height, true);
    }

    public void setResolution(int resolutionIndex)
    {
        PlayerPrefs.SetInt("resolutionIndex", resolutionIndex);
        Resolution resolution = resolutions[resolutionIndex];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
    }

    public void setFullscren(bool boolean)
    {
        Screen.fullScreen = boolean;
        PlayerPrefs.SetInt("fullscreen", boolean ? 0 : 1); //0 = true
        Debug.Log("Setou grafico: " + boolean);
    }

    public void setVolume(float volume)
    {
        audioMixer.SetFloat("volume", volume);
        PlayerPrefs.SetFloat("volume", volume);
        volumeSlider.value = volume;
    }

    public void setSensivity(float value)
    {
        PlayerPrefs.SetFloat("sensivity", value);
    }
    public void setSensivityScope(float value)
    {
        PlayerPrefs.SetFloat("sensivityScope", value);
    }

}
