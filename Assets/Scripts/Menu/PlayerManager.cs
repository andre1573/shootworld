﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.IO;

public class PlayerManager : MonoBehaviour
{
	PhotonView PV;

	GameObject controller;
	[HideInInspector] public Transform checkpointAtual;

	void Awake()
	{
		PV = GetComponent<PhotonView>();
	}

	void Start()
	{
		if(PV.IsMine)
		{
			CreateController();
		}
	}

	void CreateController()
	{
		Transform spawnpoint = SpawnManager.Instance.GetSpawnpoint();
		if (checkpointAtual != null)
        {
			spawnpoint = checkpointAtual;
        }
		
		controller = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "PlayerController"), spawnpoint.position, spawnpoint.rotation, 0, new object[] { PV.ViewID });
		controller.GetComponent<PlayerController>().resetarCaracteristicasPlayer();
	}

	public void Renascer()
    {
		Transform spawnpoint = SpawnManager.Instance.GetSpawnpoint();
		controller.transform.position = spawnpoint.position;
		controller.transform.rotation = spawnpoint.rotation;
		controller.GetComponent<PlayerController>().resetarCaracteristicasPlayer();
	}

	public void Die()
	{
		PhotonNetwork.Destroy(controller);
		CreateController();
		int qtdPlayerManager = 0;
		foreach(GameObject playerManager in GameObject.FindGameObjectsWithTag("playermanager"))
        {
			if (playerManager.GetComponent<PhotonView>().IsMine)
			{
				qtdPlayerManager++;
				if (qtdPlayerManager > 1)
				{
					PhotonNetwork.Destroy(playerManager);
				}
			}
        }
		int qtdPlayerController = 0;
		foreach (GameObject playerController in GameObject.FindGameObjectsWithTag("Player"))
		{
			if (playerController.GetComponent<PhotonView>().IsMine)
			{
				qtdPlayerController++;
				if (qtdPlayerController > 1)
				{
					PhotonNetwork.Destroy(playerController);
				}
			}
		}
	}
	
}