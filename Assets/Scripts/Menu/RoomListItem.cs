﻿using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RoomListItem : MonoBehaviour
{
	[SerializeField] TMP_Text textNameRoom, txQtdPlayer;
	[SerializeField] TMP_InputField inputPassword;
	[SerializeField] Image imgPlay;

	public RoomInfo info;

	public void SetUp(RoomInfo _info)
	{
		info = _info;
		textNameRoom.text = _info.Name;
		txQtdPlayer.text = _info.PlayerCount + "/" + _info.MaxPlayers;
	}

	public void OnClick()
	{
		Debug.Log("Senha Sala: "+info.CustomProperties["secret"]);
		Debug.Log("Senha Digitada: " + inputPassword.text);
		if (info.CustomProperties.ContainsKey("secret") && !info.CustomProperties["secret"].Equals(inputPassword.text))
		{
			imgPlay.color = Color.red;
			Debug.Log("Senha incorreta");
        }
        else
        {//verificar max players fulçl
			Launcher.Instance.JoinRoom(info);
		}
	}

	public void OnChangePasswordInput()
    {
		imgPlay.color = Color.green;
	}

}