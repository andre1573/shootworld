using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using TMPro;
using UnityEngine.UI;

public class PlayFabLeaderboard : MonoBehaviour
{
    [SerializeField] Transform leaderboardListContentTrofeusGlobal, leaderboardListContentTrofeusFriends, leaderboardListContentTimeMapsGlobal, leaderboardListContentTimeMapsFriends, leaderboardListContentBunnyhopGlobal, leaderboardListContentBunnyhopFriends;
    [SerializeField] GameObject PlayerLeaderboardItemPrefab;
    [SerializeField] TMP_Text txNomeMapa;
    [SerializeField] Mapas mapas;
    [SerializeField] Image imgMap;
    [HideInInspector] int maxScore = 0, index = 0;

    private void Start()
    {
        atualizarLeaderboard();
    }

    public void atualizarLeaderboard()
    {
        GetLeaderboardTrofeuPlayer();
        AtualizarLeaderboardDeMapas();

        GetLeaderboardTrofeuScore();
        GetLeaderboardBunnyhopScore();

        GetFriendsLeaderboardTrofeu();
        GetFriendsLeaderboardBunnyhop();

    }

    public void SendLeaderBoardTrofeuScoreForce(int score)
    {
        Debug.Log("enviando leaderboard de " + score + " trofeus");
        var request = new UpdatePlayerStatisticsRequest
        {
            Statistics = new List<StatisticUpdate>
            {
                new StatisticUpdate
                {
                    StatisticName = "TrofeuScore",
                    Value = score
                }
            }
        };
        PlayFabClientAPI.UpdatePlayerStatistics(request, OnLeaderBoardUpdate, OnError);
    }

    public void SendLeaderBoardTrofeuScore(int score)
    {
        if (!GameObject.FindGameObjectWithTag("roommanager").GetComponent<RoomManager>().competitive) return;
        Debug.Log("enviando leaderboard de "+score+" trofeus");
        var request = new UpdatePlayerStatisticsRequest
        {
            Statistics = new List<StatisticUpdate>
            {
                new StatisticUpdate
                {
                    StatisticName = "TrofeuScore",
                    Value = score
                }
            }
        };
        PlayFabClientAPI.UpdatePlayerStatistics(request, OnLeaderBoardUpdate, OnError);
    }

    public void SendLeaderBoardBunnyhopScore(int score)
    {
        Debug.Log("enviando leaderboard de " + score + " bunnyhop");
        var request = new UpdatePlayerStatisticsRequest
        {
            Statistics = new List<StatisticUpdate>
            {
                new StatisticUpdate
                {
                    StatisticName = "BunnyhopScore",
                    Value = score
                }
            }
        };
        PlayFabClientAPI.UpdatePlayerStatistics(request, OnLeaderBoardUpdate, OnError);
    }

    void OnLeaderBoardUpdate(UpdatePlayerStatisticsResult result)
    {
        Debug.Log("Sucess update leaderboard sent");
        atualizarLeaderboard();
    }
    void OnError(PlayFabError error)
    {
        Debug.LogError(error.ErrorMessage);
    }

    public void GetLeaderboardTrofeuScore()
    {
        var request = new GetLeaderboardRequest
        {
            StatisticName = "TrofeuScore",
            StartPosition = 0,
            MaxResultsCount = 100
        };
        PlayFabClientAPI.GetLeaderboard(request, OnLeaderBoardGetTrofeu, OnError);
    }

    public void GetLeaderboardBunnyhopScore()
    {
        var request = new GetLeaderboardRequest
        {
            StatisticName = "BunnyhopScore",
            StartPosition = 0,
            MaxResultsCount = 100
        };
        PlayFabClientAPI.GetLeaderboard(request, OnLeaderBoardGetBunnyhop, OnError);
    }

    void OnLeaderBoardGetTrofeu(GetLeaderboardResult result)
    {
        if (leaderboardListContentTrofeusGlobal == null) return;
        foreach (Transform child in leaderboardListContentTrofeusGlobal)
        {
            Destroy(child.gameObject);
        }
        int position = 1;
        foreach (var item in result.Leaderboard)
        {
            Instantiate(PlayerLeaderboardItemPrefab, leaderboardListContentTrofeusGlobal).GetComponent<PlayerLeaderboardItem>().SetUp(item.Profile.DisplayName, item.StatValue + "", position, "trofeu");
            position++;
        }
    }

    void OnLeaderBoardGetBunnyhop(GetLeaderboardResult result)
    {
        if (leaderboardListContentBunnyhopGlobal == null) return;
        foreach (Transform child in leaderboardListContentBunnyhopGlobal)
        {
            Destroy(child.gameObject);
        }
        int position = 1;
        foreach (var item in result.Leaderboard)
        {
            Instantiate(PlayerLeaderboardItemPrefab, leaderboardListContentBunnyhopGlobal).GetComponent<PlayerLeaderboardItem>().SetUp(item.Profile.DisplayName, item.StatValue + "", position, "coroa");
            position++;
        }
    }

    public void GetLeaderboardTrofeuPlayer()
    {
        var request = new GetPlayerStatisticsRequest
        {
            StatisticNames = new List<string>() { "TrofeuScore" }
        };
        PlayFabClientAPI.GetPlayerStatistics(request, OnLeaderboardPlayerTrofeu, OnError);
    }

    void OnLeaderboardPlayerTrofeu(GetPlayerStatisticsResult result) //Pega o placar dos players
    {
        if (result.Statistics != null && result.Statistics.Count > 0)
        {
            maxScore = result.Statistics[0].Value;
        }
        else
        {
            maxScore = 0;
        }
        if(maxScore < 0)
        {
            SendLeaderBoardTrofeuScore(maxScore * -1);
            maxScore = maxScore * -1;
        }
        PlayerPrefs.SetInt("TROFEUS", maxScore);
        Debug.Log("Sucessleaderboard trofeu player: " + maxScore);
    }

    public void GetFriendsLeaderboardTrofeu()
    {
        var request = new GetFriendLeaderboardAroundPlayerRequest
        {
            StatisticName = "TrofeuScore",
            IncludeSteamFriends = true
        };
        PlayFabClientAPI.GetFriendLeaderboardAroundPlayer(request, OnFriendsLeaderboardTrofeu, OnError);
    }

    public void GetFriendsLeaderboardBunnyhop()
    {
        var request = new GetFriendLeaderboardAroundPlayerRequest
        {
            StatisticName = "BunnyhopScore",
            IncludeSteamFriends = true
        };
        PlayFabClientAPI.GetFriendLeaderboardAroundPlayer(request, OnFriendsLeaderboardBunnyhop, OnError);
    }

    void OnFriendsLeaderboardTrofeu(GetFriendLeaderboardAroundPlayerResult result)
    {
        if (leaderboardListContentTrofeusFriends == null) return;
        foreach (Transform child in leaderboardListContentTrofeusFriends)
        {
            Destroy(child.gameObject);
        }
        int position = 1;
        foreach (var item in result.Leaderboard)
        {
            Instantiate(PlayerLeaderboardItemPrefab, leaderboardListContentTrofeusFriends).GetComponent<PlayerLeaderboardItem>().SetUp(item.Profile.DisplayName, item.StatValue + "", position, "trofeu");
            position++;
        }
    }

    void OnFriendsLeaderboardBunnyhop(GetFriendLeaderboardAroundPlayerResult result)
    {
        if (leaderboardListContentBunnyhopFriends == null) return;
        foreach (Transform child in leaderboardListContentBunnyhopFriends)
        {
            Destroy(child.gameObject);
        }
        int position = 1;
        foreach (var item in result.Leaderboard)
        {
            Instantiate(PlayerLeaderboardItemPrefab, leaderboardListContentBunnyhopFriends).GetComponent<PlayerLeaderboardItem>().SetUp(item.Profile.DisplayName, item.StatValue + "", position, "coroa");
            position++;
        }
    }

    public void GiveGems(int value)
    {
        AddUserVirtualCurrencyRequest request = new AddUserVirtualCurrencyRequest
        {
            Amount = value,
            VirtualCurrency = "GE"
        };
        PlayerPrefs.SetInt("GEMS", PlayerPrefs.GetInt("GEMS") + value);
        PlayFabClientAPI.AddUserVirtualCurrency(request, OnGiveGemsComplete, OnError);
    }

    private void OnGiveGemsComplete(ModifyUserVirtualCurrencyResult result) 
    {
        Debug.Log("Recebeu 1 gema com sucesso");
    }

    //------------------------LEADERBOARD MAPAS-------------------------------

    public void SendLeaderBoardTempoMapa(int score, string mapaName)
    {
        Debug.Log("enviando leaderboard de " + score + " segundos do mapa: "+ mapaName);
        var request = new UpdatePlayerStatisticsRequest
        {
            Statistics = new List<StatisticUpdate>
            {
                new StatisticUpdate
                {
                    StatisticName = mapaName,
                    Value = score
                }
            }
        };
        PlayFabClientAPI.UpdatePlayerStatistics(request, OnLeaderBoardUpdate, OnError);
    }


    public void GetLeaderboardTempoMapa()
    {
        var request = new GetLeaderboardRequest
        {
            StatisticName = "Game"+index,
            StartPosition = 0,
            MaxResultsCount = 100
        };
        PlayFabClientAPI.GetLeaderboard(request, OnLeaderboardTempoMapa, OnError);
    }

    void OnLeaderboardTempoMapa(GetLeaderboardResult result)
    {
        if (leaderboardListContentTimeMapsGlobal == null) return;
        foreach (Transform child in leaderboardListContentTimeMapsGlobal)
        {
            Destroy(child.gameObject);
        }
        int position = 1;
        result.Leaderboard.Reverse();
        foreach (var item in result.Leaderboard)
        {
            Instantiate(PlayerLeaderboardItemPrefab, leaderboardListContentTimeMapsGlobal).GetComponent<PlayerLeaderboardItem>().SetUp(item.Profile.DisplayName, item.StatValue + " s", position, "relogio");
            position++;
        }
    }

    private void AtualizarLeaderboardDeMapas()
    {
        if (mapas == null || txNomeMapa == null) return;
        txNomeMapa.text = mapas.txMapas[index];
        imgMap.sprite = mapas.imgMapas[index];
        GetLeaderboardTempoMapa();
    }

    public void rightSelectionMap()
    {
        index++;
        if (index >= mapas.imgMapas.Length) index = 0;
        AtualizarLeaderboardDeMapas();
    }

    public void leftSelectionMap()
    {
        index--;
        if (index < 0) index = mapas.imgMapas.Length - 1;
        AtualizarLeaderboardDeMapas();
    }

}
