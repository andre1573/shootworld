using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Globalization;
using TMPro;

public class ItemShop : MonoBehaviour
{
	public Sprite itemImage;
	public string itemName;
	public string itemCost;
	public string itemDesc;

	public Image itemImageField;
	public TMP_Text itemNameTextField, itemCostTextField, itemDescTextField;
	public Button buyButton;
	public bool inPagamento = false;

	// Use this for initialization
	void Start()
	{
		inPagamento = false;
		if (itemImage == null)
		{
			itemImage = Resources.Load<Sprite>("ItemSprites/DefaultImage");
		}

		itemImageField.sprite = itemImage;

		if (itemName.Length > 100)
		{
			itemName = itemName.Substring(0, 99);
		}

		itemNameTextField.text = itemName;

		string txCost = string.Format("{0:N}", itemCost);
		itemCostTextField.text = "USD" + txCost;

		//itemCurCodeTextField.text = "(" + GlobalPayPalProperties.INSTANCE.currencyCode + ")";

		itemDescTextField.text = itemDesc;

	}

    private void LateUpdate()
    {
		string txCost = string.Format("{0:N}", itemCost);
		itemCostTextField.text = "USD" + txCost;
	}

}
