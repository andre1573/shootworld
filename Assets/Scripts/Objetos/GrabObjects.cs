using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabObjects : MonoBehaviour
{

    private string tagObjeto = "objeto";
    [SerializeField] Camera cam;
    [HideInInspector] public GameObject grabedObj;
    public float forceGrab = 8.0f;
    public float maxDistancePlayer = 0.7f, maxDistanceObjeto = 3.0f;
    private Vector2 rigSaveGrabed;
    public GameObject grabRoot;

    // Start is called before the first frame update
    void Start()
    {
        grabedObj = null;
    }

    // Update is called once per frame
    void Update()
    {
        Ray ray = cam.ViewportPointToRay(new Vector3(0.5f, 0.5f));
        ray.origin = new Vector3(grabRoot.transform.position.x, grabRoot.transform.position.y, grabRoot.transform.position.z);

        Debug.DrawRay(ray.origin, ray.direction, Color.blue);

        if (Physics.Raycast(ray, out RaycastHit hit, maxDistanceObjeto))
        {
            if (hit.transform.tag == tagObjeto)
            {
                Debug.Log("RAY PEGOU NO OBJETO");
                if (Input.GetMouseButtonDown(0))
                {
                    grabedObj = hit.transform.gameObject;
                }
            }
        }

        this.pegarObjeto();
        

    }

    private void pegarObjeto()
    {
        if (grabedObj != null)
        {
            if (!grabedObj.GetComponent<Rigidbody>())
            {
                Debug.LogError("Coloque um Rigidbody no objeto!");
                return;
            }

            Rigidbody objRig = grabedObj.GetComponent<Rigidbody>();
            Vector3 posGrab = grabRoot.transform.position + grabRoot.transform.forward * maxDistancePlayer;
            float dist = Vector3.Distance(grabedObj.transform.position, posGrab);
            float calc = forceGrab * dist * 6 * Time.deltaTime;

            if (rigSaveGrabed == Vector2.zero)
                rigSaveGrabed = new Vector2(objRig.drag, objRig.angularDrag);
            objRig.drag = 2.5f;
            objRig.angularDrag = 2.5f;
            objRig.AddForce(-(grabedObj.transform.position - posGrab).normalized * calc, ForceMode.Impulse);


            if (Input.GetMouseButtonUp(0) || objRig.velocity.magnitude >= 20 || dist >= 10 || grabedObj.transform.position.y > 40)
            {
                UngrabObject();
            }

        }
    }

    void UngrabObject()
    {
        Rigidbody objRig = grabedObj.GetComponent<Rigidbody>();
        objRig.drag = rigSaveGrabed.x;
        objRig.angularDrag = rigSaveGrabed.y;
        rigSaveGrabed = Vector2.zero;
        grabedObj = null;
    }

}
