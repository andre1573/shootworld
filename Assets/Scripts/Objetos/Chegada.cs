using UnityEngine;
using Photon.Pun;

public class Chegada : MonoBehaviourPunCallbacks
{

    PhotonView PV;

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            other.GetComponent<ControleCorrida>().vencedor = true;
            other.GetComponent<ControleCorrida>().PlayerChegouNaChegadaFinal(true);
        }
    }

    private void Awake()
    {
        PV = GetComponent<PhotonView>();
    }
    // Start is called before the first frame update
  
}
