using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.IO;

public class PistasBunnyhopControle : MonoBehaviour
{

    [SerializeField] GameObject pistaAtual, chaoAssassino;
    List<GameObject> pistasPassadas;
    [SerializeField] int qtdPistas;
    // Start is called before the first frame update

    private void Awake()
    {
        pistasPassadas = new List<GameObject>();
    }

    private void Start()
    {
        ReiniciarPistas();
    }

    public void CriarNovaPista()
    {
        Vector3 posicao = new Vector3(pistaAtual.transform.position.x, pistaAtual.transform.position.y+10, pistaAtual.transform.position.z - 400);
        pistaAtual = PhotonNetwork.Instantiate(Path.Combine("BunnyhopPistas", "pista"+Random.Range(0, qtdPistas)), posicao, pistaAtual.transform.rotation);
        pistasPassadas.Add(pistaAtual);
        removerPistasAntigas();
    }

    private void removerPistasAntigas()
    {
        if(pistasPassadas.Count >= 4)
        {
            Destroy(pistasPassadas[0]);
            pistasPassadas.RemoveAt(0);
        }
    }

    public void ReiniciarPistas()
    {
        chaoAssassino.GetComponent<ChaoAssassino>().ReiniciarPosicaoBaseChaoAssassino();
        pistaAtual = PhotonNetwork.Instantiate(Path.Combine("BunnyhopPistas", "pista0"), new Vector3(0, 0, 0), new Quaternion(0, 0, 0, 0));
        for (int i=0; i<pistasPassadas.Count; i++)
        {
            Destroy(pistasPassadas[i]);
        }
        pistasPassadas = new List<GameObject>();
        pistasPassadas.Add(pistaAtual);
    }

}
