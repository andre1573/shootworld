using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColocacaoPlayer : MonoBehaviour
{

    [HideInInspector] public string nome, colocao;

    public ColocacaoPlayer(string nome, string colocao)
    {
        this.nome = nome;
        this.colocao = colocao;
    }
}
