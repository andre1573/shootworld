using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasLookAt : MonoBehaviour
{
    // Start is called before the first frame update
    private Canvas canvas;
    public GameObject go;
    private CanvasGroup canvasGroup;
    void Start()
    {
        canvas = GetComponent<Canvas>();
        canvasGroup = GetComponent<CanvasGroup>();
    }

    // Update is called once per frame
    void Update()
    {
        if(canvas.worldCamera != null)
        {
            go.transform.LookAt(canvas.worldCamera.transform);
            go.transform.Rotate(Vector3.up * 180);
            float deltaAngle = Mathf.DeltaAngle(canvas.worldCamera.transform.rotation.eulerAngles.y, go.transform.rotation.eulerAngles.y);
            canvasGroup.alpha = Mathf.Clamp01(180f - Mathf.Abs(deltaAngle) - 130f / 130 - 160); 
        }
    }
}
