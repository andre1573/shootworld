using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.SceneManagement;

public class ControleCorrida : MonoBehaviourPunCallbacks
{

    [HideInInspector] public bool chegouNaChegadaFinal = false, vencedor = false;
    PlayFabLeaderboard playFabLeaderboard;
    PhotonView PV;

    private ColocacaoControle colocacaoControle;

    // Start is called before the first frame update
    void Awake()
    {
        Debug.Log("Current lobby: "+PhotonNetwork.CurrentLobby.Name);
        vencedor = false;
        PV = GetComponent<PhotonView>();
        playFabLeaderboard = GetComponent<PlayFabLeaderboard>();
    }

    private void Update()
    {
        if(colocacaoControle == null)
        {
            GameObject obj = GameObject.FindGameObjectWithTag("colocacaocontrole");
            if (obj != null)
            {
                colocacaoControle = obj.GetComponent<ColocacaoControle>();
            }
        }
    }

    public void PlayerChegouNaChegadaFinal(bool chegouNoFimDoMapa)
    {
        if (chegouNaChegadaFinal) return;
        chegouNaChegadaFinal = true;
        int tempoCronometroFim = ((int)colocacaoControle.cronometroPartida);
        if (chegouNoFimDoMapa)
        {
            playFabLeaderboard.SendLeaderBoardTempoMapa(tempoCronometroFim, SceneManager.GetActiveScene().name);
        }
        playFabLeaderboard.SendLeaderBoardTrofeuScore(30); //Vencedor do 1v1 ganha trofeus //SE ALTERAR O VALOR AQUI PRECISA ALTERAR NA TEAL DE FIM DE JOGO PQ TA FIXO
        playFabLeaderboard.GiveGems(1);
        Debug.Log("O jogador "+ PV.Owner.NickName + " chegou no final do mapa, finalizando game de todos...");
        vencedor = true;
        PV.RPC("RPC_AlguemChegouNaChegadaFinal", RpcTarget.All, PV.Owner.NickName, PV.Owner.UserId, tempoCronometroFim); //O jogoador chegou no final do mapa, finalizando game de todos...
    }

    public void PlayerMorreuNoModoBunnyhop()
    {
        Debug.Log("Player morreu no modo bunnyhop");
        ColocacaoControle objColocacao = GameObject.FindGameObjectWithTag("colocacaocontrole").GetComponent<ColocacaoControle>();
        playFabLeaderboard.SendLeaderBoardBunnyhopScore((int)objColocacao.score);
        objColocacao.AbrirHudScoreBunnyhop();
    }

    public void PlayerKitouDoCompetitivo() //Ja tira o trofeu do player que kitou e manda ele pra tela de fim de jogo LOSE
    {
        Debug.Log("Voce kitou");
        playFabLeaderboard.SendLeaderBoardTrofeuScore(-20); //qm kita do 1v1 perde trofeus //SE ALTERAR O VALOR AQUI PRECISA ALTERAR NA TEAL DE FIM DE JOGO PQ TA FIXO
        colocacaoControle.PreencherJogadoresDaCorrida("", ((int)colocacaoControle.cronometroPartida));
        PlayerPrefs.SetString("vId", "");
        PlayerPrefs.SetInt("inpartida", 0);
        PhotonNetwork.AutomaticallySyncScene = false;
        PhotonNetwork.LoadLevel("FimDeJogo");
    }

    [PunRPC]
    void RPC_AlguemChegouNaChegadaFinal(string nomeVencedor, string userId, int tempoCronometroFim)
    {
        if (!vencedor)
        {
            playFabLeaderboard.SendLeaderBoardTrofeuScore(-20); //Perdedor do 1v1 perde trofeus //SE ALTERAR O VALOR AQUI PRECISA ALTERAR NA TEAL DE FIM DE JOGO PQ TA FIXO
        }
        colocacaoControle.PreencherJogadoresDaCorrida(nomeVencedor+ " Wins!", tempoCronometroFim);
        PlayerPrefs.SetString("vId", userId);
        PlayerPrefs.SetInt("inpartida", 0);
        SceneManager.LoadScene("FimDeJogo");
    }

}
