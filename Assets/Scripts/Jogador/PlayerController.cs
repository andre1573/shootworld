using Photon.Pun;
using UnityEngine;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using Leguar.HiddenVars;

public class PlayerController : MonoBehaviourPunCallbacks
{
	public GameObject ui, cameraPadrao, cameraHolder;
	[SerializeField] float mouseSensitivity;
	float tempoEmpurrao = 0.3f, timeLeftEmpurrao, tempoBunnando = 1;
	[HideInInspector] public bool grounded, canMove = true;
	PhotonView PV;
	[HideInInspector] public PlayerManager playerManager;
	public GameObject enemyTag;
	[HideInInspector] public HiddenVars hiddenVars;
	CharacterController characterController;
	Vector3 moveDirection = Vector3.zero;
	private float horizontalMove, verticalMove;
	[HideInInspector] public bool sendoEmpurrado = false;
	[HideInInspector] public Vector3 empurraoDir;
	[HideInInspector] public float empurraoForce;
	public bool isBOT = false, godMOD;

	public AudioSource soundJump;

	void Awake()
	{
		hiddenVars = new HiddenVars();
		characterController = GetComponent<CharacterController>();
		PV = GetComponent<PhotonView>();
		if (isBOT) return;
		playerManager = PhotonView.Find((int)PV.InstantiationData[0]).GetComponent<PlayerManager>();
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
	}

	void Start()
	{
		if (PV.IsMine)
		{
			Hashtable hash = new Hashtable();
			hash.Add("personagemskin", PlayerPrefs.GetInt("personagemskin"));
			PhotonNetwork.LocalPlayer.SetCustomProperties(hash);
			resetarCaracteristicasPlayer();
			Destroy(enemyTag);
		}
		else
		{
			PlayerController[] players = FindObjectsOfType<PlayerController>();
			if (players.Length == 0) return;
			foreach (PlayerController player in players)
			{
				if (player.PV.IsMine)
				{
					enemyTag.GetComponent<Canvas>().worldCamera = player.cameraPadrao.GetComponent<Camera>();
				}
			}
			Destroy(cameraPadrao);
			Destroy(ui);
		}
	}

	void Update()
	{
		if (!PV.IsMine || !canMove) return;
		Look();
		SendoEmpurrado();
        characterController.Move(moveDirection * Time.deltaTime);
		Move();
		AcelerarVelocidadeBunnyhop();
		if (transform.position.y < -30f ) // Die if you fall out of the world
		{
			Die(false);
		}
		if (PlayerPrefs.GetFloat("sensivity") <= 0) mouseSensitivity = 2.0f;
		else mouseSensitivity = PlayerPrefs.GetFloat("sensivity");

        //RETIRAR GOD MOD DEPOIS
		/*if (Input.GetKeyDown(KeyCode.F))
        {
			godMOD = !godMOD;
        }*/
	}

	void Look()
	{
		if (isBOT || cameraPadrao == null) return;
		transform.Rotate(Vector3.up * Input.GetAxisRaw("Mouse X") * mouseSensitivity);
	}

	void SendoEmpurrado()
    {
        if (sendoEmpurrado)
        {
			GetComponent<Animator>().SetBool("empurrado", true);
			if (timeLeftEmpurrao < 0)
			{
				moveDirection.y -= 20.0f * Time.deltaTime; //gravidade
				sendoEmpurrado = false;
				timeLeftEmpurrao = tempoEmpurrao;
				GetComponent<Animator>().SetBool("empurrado", false);
			}
			timeLeftEmpurrao -= Time.deltaTime;
		}
	}

	[PunRPC]
	void RPC_Empurrao(Vector3 empurraoDir, float empurraoForce)
	{
		if (!PV.IsMine) return;
		this.sendoEmpurrado = true;
		this.empurraoDir = empurraoDir;
		this.empurraoForce = empurraoForce;
	}

	void Move()
	{
		Vector3 forward = transform.TransformDirection(Vector3.forward);
		Vector3 right = transform.TransformDirection(Vector3.right);
		float velocidadeChar = hiddenVars.GetInt("walkSpeed") + hiddenVars.GetFloat("velBunnyhop");
		if (godMOD) velocidadeChar = 20;
		float curSpeedX = velocidadeChar * Input.GetAxis("Vertical");
		float curSpeedY = velocidadeChar * Input.GetAxis("Horizontal");
		float movementDirectionY = moveDirection.y;
		if (sendoEmpurrado)
		{
			moveDirection = (empurraoDir * (empurraoForce*5f));
		}
        else
        {
			moveDirection = (forward * curSpeedX) + (right * curSpeedY);
		}

		if (Input.GetButtonDown("Fire2"))
		{
			cameraHolder.transform.localPosition = new Vector3(0, 12.53003f, 5.81f);
			cameraHolder.transform.localRotation = Quaternion.Euler(30, 0, 0);
			cameraPadrao.transform.localPosition = new Vector3(0, -10, 3);
			cameraPadrao.transform.localRotation = Quaternion.Euler(50, -180, 0);
		}
		else if (Input.GetButtonUp("Fire2"))
		{
			cameraHolder.transform.localPosition = new Vector3(0, 12.53003f, 0);
			cameraHolder.transform.localRotation = Quaternion.Euler(30, 0, 0);
			cameraPadrao.transform.localPosition = new Vector3(0, -10, 3);
			cameraPadrao.transform.localRotation = Quaternion.Euler(-10, 0, 0);
		}

		horizontalMove = Input.GetAxisRaw("Horizontal");
		verticalMove = Input.GetAxisRaw("Vertical");
		GetComponent<Animator>().SetFloat("Horizontal", horizontalMove);
		GetComponent<Animator>().SetFloat("Vertical", verticalMove);

		if (Input.GetButtonUp("Jump"))
        {
			hiddenVars.SetFloat("velBunnyhop", 1);
        }
		if (Input.GetButton("Jump") && characterController.isGrounded)
		{
			moveDirection.y = hiddenVars.GetInt("jumpForce");
			if (soundJump.isPlaying) soundJump.Stop();
			soundJump.Play();
		}
		else
		{
			moveDirection.y = movementDirectionY;
		}
		if (!characterController.isGrounded)
		{
			GetComponent<Animator>().SetBool("pulando", true);
			if(!godMOD) moveDirection.y -= 20.0f * Time.deltaTime; //gravidade
			else moveDirection.y -= 3.0f * Time.deltaTime; //gravidade
		}
        else
        {
			GetComponent<Animator>().SetBool("pulando", false);
		}
		if (verticalMove == 0)
		{
			hiddenVars.SetFloat("velBunnyhop", 1);
		}

	}
	public void SetGroundedState(bool _grounded)
	{
		grounded = _grounded;
	}

	public void Die(bool inModoBunnyhop)
	{
        if (inModoBunnyhop)
        {
			canMove = false;
			GetComponent<ControleCorrida>().PlayerMorreuNoModoBunnyhop();
        }
        else
        {
			playerManager.Die();
		}
	}

	public void resetarCaracteristicasPlayer()
    {
		tempoBunnando = 0;
		hiddenVars.SetInt("walkSpeed", 4);
		hiddenVars.SetInt("jumpForce", 10);
		hiddenVars.SetFloat("velBunnyhop", 1);
		sendoEmpurrado = false;
		timeLeftEmpurrao = tempoEmpurrao;

		cameraHolder.transform.localPosition = new Vector3(0, 12.53003f, 0.41f);
		cameraHolder.transform.localRotation = Quaternion.Euler(30, 0, 0.41f);
		cameraPadrao.transform.localPosition = new Vector3(0, -10, 3);
		cameraPadrao.transform.localRotation = Quaternion.Euler(-10, 0, 0);
	}

	private void AcelerarVelocidadeBunnyhop()
    {
		if(tempoBunnando < 0)
        {
			if(hiddenVars.GetFloat("velBunnyhop") <= 20)
            {
				hiddenVars.SetFloat("velBunnyhop", hiddenVars.GetFloat("velBunnyhop") + 0.05f + (hiddenVars.GetFloat("velBunnyhop") * 0.05f));
			}
			tempoBunnando = 1;
		}
        if (Input.GetButton("Jump") && (verticalMove != 0 || horizontalMove != 0))
        {
			tempoBunnando -= Time.deltaTime;
		}
	}

}